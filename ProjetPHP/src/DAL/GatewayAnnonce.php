<?php
/**
 * Created by PhpStorm.
 * User: alfarjas
 * Date: 03/02/20
 * Time: 11:34
 */

class GatewayAnnonce
{
    private $con;

    //Constructeur
    public function __construct(Connection $con)
    {
        $this->con=$con;
    }

    public function getAnnonce(){
        $query='SELECT * FROM Annonce';
    }

    public function getLast3AnnoncesByDate(){
        $query='SELECT * FROM Annonce ORDER BY datePublication DESC LIMIT 3 ';
        $this->con->executeQuery($query);
        return $this->con->getResults();
    }


}