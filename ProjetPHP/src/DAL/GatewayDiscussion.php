<?php
/**
 * Created by PhpStorm.
 * User: alfarjas
 * Date: 02/12/19
 * Time: 09:10
 */

class GatewayDiscussion
{
    private $con;

    //Constructeur
    public function __construct(Connection $con)
    {
        $this->con=$con;
    }

    public function getDiscussion()
    {
        $query='SELECT * FROM Discussion ORDER BY nom';
        $this->con->executeQuery($query);
        $tab = $this->con->getResults();
        $tab2 = array();
        foreach ($tab as $dis)
        {
            $discussion = new Topic($dis['idDiscussion'],$dis['nom'],$dis['idUser']);
            $tab2[] = $discussion;
        }
        return $tab2;
    }

    public function addDiscussion($nom, $mailUser)
    {
        $idTopic= null;
        $query='INSERT INTO Topic VALUES(:idTopic,:nom,:mailUser)';
        $this->con->executeQuery($query, array(':idDiscussion'=> array($idTopic,PDO::PARAM_INT),
                                               ':nom'=> array($nom,PDO::PARAM_STR),
                                               ':mailUser'=> array($mailUser,PDO::PARAM_STR)));
    }

    public function delTopic($idDiscussion)
    {
        $query='DELETE FROM Discussion WHERE idDiscussion=:idDiscussion';
        $this->con->executeQuery($query, array(':idDiscussion'=> array($idDiscussion,PDO::PARAM_STR)));
    }
}
