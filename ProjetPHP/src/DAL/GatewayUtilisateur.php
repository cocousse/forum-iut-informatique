<?php

class GatewayUtilisateur
{
    private $con;

    //Constructeur
    public function __construct(Connection $con)
    {
        $this->con=$con;
    }

    //Insere dans la base un user, quand il l'instancie
    function insererUser($user)
    {
        $rien="";
        $type=$user->getType();
        $mdp=$user->getMdp();
        $email=$user->getEmail();
        $nom=$user->getNom();
        $prenom=$user->getPrenom();
        $description=$user->getDescription();
        $nbMessage=$user->getNbMessage();
        $avatar = null;
        $isMute = false;


        $query='INSERT INTO Utilisateur VALUES(:rien, :type, :mdp, :email, :nom, :prenom, :description, :nbMessage, :avatar, :isMute)';



        try
        {
            $this->con->executeQuery($query,array(':rien'=>array($rien,PDO::PARAM_INT),
                                                  ':type'=>array($type,PDO::PARAM_INT),
                                                  ':mdp'=>array($mdp,PDO::PARAM_STR),
                                                  ':email'=>array($email,PDO::PARAM_STR),
                                                   ':nom'=>array($nom,PDO::PARAM_STR),
                                                    ':prenom'=>array($prenom,PDO::PARAM_STR),
                                                    ':description'=>array($description,PDO::PARAM_STR),
                                                    ':nbMessage'=>array($nbMessage,PDO::PARAM_INT),
                                                    ':avatar'=>array($avatar,PDO::PARAM_STR),
                                                    ':isMute'=>array($isMute,PDO::PARAM_BOOL)));

        }
        catch(PDOException $e)
        {
            throw new Exception($e->getMessage());
        }

    }

    //Créer une instance Utilisateur en fonction du mail rentré en paramètre
    function findByEmail($email)
    {
        $query = 'SELECT * FROM Utilisateur WHERE email=:email';
        try
        {
            $this->con->executeQuery($query, array(':email'=> array($email,PDO::PARAM_STR)));
        }
        catch (PDOException $e)
        {
            throw new Exception("Erreur SQL");
        }
        return $this->con->getResults();
    }

    function changerTypeUser($idUser, $type)
    {
        $query = 'UPDATE Utilisateur SET typeUser=:rang where idUser=:idUser;';
        try {
            $this->con->executeQuery($query, array(':rang' => [$type, PDO::PARAM_INT],
                ':idUser' => [$idUser, PDO::PARAM_INT]));
        } catch (PDOException $e) {
            throw new Exception("Erreur SQL");
        }
    }




        function Connexion($email, $motdepasse)
       {
        $query='SELECT mdpUser FROM Utilisateur WHERE email=:email';
        try
        {
            $this->con->executeQuery($query,array(':email'=>array($email,PDO::PARAM_STR)));
        }
        catch(Exception $e)
        {
            //Vue erreur à faire
            throw new Exception($e->getMessage());

            throw new Exception("Erreur SQL dans la connexion");
        }

        $res = $this->con->getResults();
        if(password_verify($motdepasse, current(current($res))))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }

    function getType($email)
    {
        $query='SELECT typeUser FROM Utilisateur WHERE email=:email';
        try
        {
            $this->con->executeQuery($query,array(':email'=>array($email,PDO::PARAM_STR)));
        }
        catch(Exception $e)
        {
            //Vue erreur à faire
            throw new Exception($e->getMessage());

            throw new Exception("Erreur SQL dans la connexion");
        }
        return $this->con->getResults();
    }

    function findUsers()
    {
        try {
            $query = 'SELECT * FROM Utilisateur';
            $this->con->executeQuery($query);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
        return $this->con->getResults();
    }

    function getnamebyID($id)
    {
        $query='SELECT nom,prenom from Utilisateur where idUser=:id';
        $this->con->executeQuery($query,array(":id"=>array($id,PDO::PARAM_INT)));
        return $this->con->getResults();
    }

    function deleteUser($idUser){
        $query ='DELETE FROM Utilisateur WHERE idUser=:idUser';
        $this->con->executeQuery($query,array(":idUser"=>array($idUser,PDO::PARAM_INT)));
    }

    function getIsMute($idUser){
        $query = 'SELECT isMute FROM Utilisateur WHERE idUser=:idUser';
        $this->con->executeQuery($query,array(":idUser"=>array($idUser,PDO::PARAM_INT)));
        return $this->con->getResults();
    }

    function muteUser($idUser){
        $isMute = $this->getIsMute($idUser);
        if(!current($isMute)[0])
            $query = 'UPDATE Utilisateur SET isMute=true WHERE idUser=:idUser';
        else
            $query = 'UPDATE Utilisateur SET isMute=false WHERE idUser=:idUser';
        $this->con->executeQuery($query,array(":idUser"=>array($idUser,PDO::PARAM_INT)));
    }

}
