<?php
/**
 * Created by PhpStorm.
 * User: alfarjas
 * Date: 02/12/19
 * Time: 09:10
 */

class GatewaySousTopic
{
    private $con;

    //Constructeur
    public function __construct(Connection $con)
    {
        $this->con=$con;
    }

    public function getSousTopic()
    {
        $query='SELECT * FROM SousTopic ORDER BY nom';
        $this->con->executeQuery($query);
        $tab = $this->con->getResults();
        $tab2 = array();
        foreach ($tab as $sous)
        {
            $sousTopic = new Topic($sous['idTopic'],$sous['nom'],$sous['idUser']);
            $tab2[] = $sousTopic;
        }
        return $tab2;
    }

    public function addSousTopic($nom, $idUser)
    {
        $idSousTopic = null;
        $query='INSERT INTO SousTopic VALUES(:idSousTopic,:nom,:idUser)';
        $this->con->executeQuery($query, array(':idSousTopic'=> array($idSousTopic,PDO::PARAM_INT),
                                               ':nom'=> array($nom,PDO::PARAM_STR),
                                               ':idUser'=> array($idUser,PDO::PARAM_STR)));
    }

    public function delSousTopic($idSousTopic)
    {
        $query='DELETE FROM Topic WHERE idSousTopic=:idSousTopic';
        $this->con->executeQuery($query, array(':idSousTopic'=> array($idSousTopic,PDO::PARAM_STR)));
    }


}
