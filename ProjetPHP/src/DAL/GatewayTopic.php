<?php
/**
 * Created by PhpStorm.
 * User: alfarjas
 * Date: 02/12/19
 * Time: 09:10
 */

class GatewayTopic
{
    private $con;

    //Constructeur
    public function __construct(Connection $con)
    {
        $this->con=$con;
    }

    public function getTopics()
    {
        $query = 'SELECT * FROM Topic ORDER BY nom';
        $this->con->executeQuery($query);
        $tab = $this->con->getResults();
        return $tab;
    }

    public function addTopic($nom, $mailUser)
    {
        $idTopic= null;
        $query='INSERT INTO Topic VALUES(:idTopic,:nom,:mailUser)';
        $this->con->executeQuery($query, array(':idTopic'=> array($idTopic,PDO::PARAM_INT),
                                               ':nom'=> array($nom,PDO::PARAM_STR),
                                               ':mailUser'=> array($mailUser,PDO::PARAM_STR)));
    }

    public function delTopic($idTopic)
    {
        $query='DELETE FROM Topic WHERE idTopic=:idTopic';
        $this->con->executeQuery($query, array(':idTopic'=> array($idTopic,PDO::PARAM_STR)));
    }

    public function getIdByName($matiere)
    {
        $query="SELECT idTopic FROM Topic where nom=:matiere";
        $this->con->executeQuery($query, array(':matiere'=> array($matiere,PDO::PARAM_STR)));
        return $this->con->getResults();
    }
}
