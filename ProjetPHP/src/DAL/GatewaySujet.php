<?php
/**
 * Created by PhpStorm.
 * User: clthinon
 * Date: 02/03/20
 * Time: 11:06
 */

class GatewaySujet
{
    private $con;
    /**
     * GatewaySujet constructor.
     * @param Connection $param
     */
    public function __construct(Connection $con)
    {
        $this->con=$con;
    }

    public function getSujet($idTopic)
    {
        $query="SELECT * FROM  Sujet WHERE idTopic=:idTopic";
        $this->con->executeQuery($query, array(':idTopic'=> array($idTopic,PDO::PARAM_STR)));
        return $this->con->getResults();
    }



    public function addSujet($nom, $idUser)
    {
        $idSousTopic = null;
        $query='INSERT INTO Sujet VALUES(:idSousTopic,:nom,:idUser)';
        $this->con->executeQuery($query, array(':idSousTopic'=> array($idSousTopic,PDO::PARAM_INT),
            ':nom'=> array($nom,PDO::PARAM_STR),
            ':idUser'=> array($idUser,PDO::PARAM_STR)));
    }

    public function delSujet($idSousTopic)
    {
        $query='DELETE FROM Topic WHERE idSujet=:idSujet';
        $this->con->executeQuery($query, array(':idSousTopic'=> array($idSousTopic,PDO::PARAM_STR)));
    }

    public function delSujetByidUser($idUser)
    {
        $query='DELETE FROM SousTopic WHERE idUser=:idUser';
        $this->con->executeQuery($query, array(':idUser'=> array($idUser,PDO::PARAM_INT)));
    }

}