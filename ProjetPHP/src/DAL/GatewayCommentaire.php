<?php
/**
 * Created by PhpStorm.
 * User: alfarjas
 * Date: 02/12/19
 * Time: 09:10
 */



class GatewayCommentaire
{
    private $con;

    //Constructeur
    public function __construct(Connection $con)
    {
        $this->con=$con;

    }

    //Créer une instance Utilisateur en fonction du mail rentré en paramètre
    function getMessageParSujet($idSujet){
        $query='SELECT * FROM Message WHERE idSujet=:idSujet and idUserDestination=null ORDER BY datePub';
        $this->con->executeQuery($query, array(':idSujet'=> array($idSujet,PDO::PARAM_INT)));




        $tab = $this->con->getAttribute();


        $tab2 =query(NULL);
        foreach ($tab as $mess)
        {
            $message=new Message();
            $message->idMessage=$mess;
            //REMPLIR TOUT DANS $message LE MESSAGE DANS TAB2
        }
        return $tab2; //Retourne un tableau de tous les messages instanciés
    }


    public function ecrireCpb($message,$nom,$prenom, $id, $idSujet)
    {
        $idCommentaire="";
        $date2=date("d m Y");
        $query='INSERT into Commentaire values(:idCommentaire,:contenu,:iduser,:idSujet,:date2,:prenom,:nom)';
        $this->con->executeQuery($query,array(':idCommentaire'=>array($idCommentaire,PDO::PARAM_INT),
            ':contenu'=>array($message,PDO::PARAM_STR),
            ':iduser'=>array($id,PDO::PARAM_INT),
            ':idSujet'=>array($idSujet,PDO::PARAM_INT),
            ':date2'=>array($date2,PDO::PARAM_STR),
            ':nom'=>array($nom,PDO::PARAM_STR),
            ':prenom'=>array($prenom,PDO::PARAM_STR)));
    }

    public function listerCpb($idSousSujet)
    {
        $query='SELECT * FROM Commentaire WHERE idSujet=:idSujet';
        $this->con->executeQuery($query, array(':idSujet'=>array($idSousSujet,PDO::PARAM_INT)));
        return $this->con->getResults();
    }

    public function deleteCommentaires($idUser){
        $query ='DELETE FROM Commentaire WHERE idUser=:idUser';
        $this->con->executeQuery($query,array(":idUser"=>array($idUser,PDO::PARAM_INT)));
    }



}
