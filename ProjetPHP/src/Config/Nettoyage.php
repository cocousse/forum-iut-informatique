<?php

class Nettoyage {

    static function val_action($action) {

        if (!isset($action)) {
            throw new Exception('pas d\'action');
            //on pourrait aussi utiliser
            //$action = $_GET['action'] ?? 'no';
            // This is equivalent to:
            //$action =  if (isset($_GET['action'])) $action=$_GET['action']  else $action='no';
        }
    }

    static function nettoyer_string($string)
    {
        if(!isset($prenom)||$prenom="")
        {
            throw new Exception("Champ vide");
        }
        return filter_var($string, FILTER_SANITIZE_STRING);
    }
}
?>