<?php

class Validation {

    static function val_inscription($prenom, $nom, $email, $mdp, &$bio)
    {
        if(!isset($prenom))
        {
            throw new Exception("Champ prénom manquant");
        }
        if($prenom != filter_var($prenom, FILTER_SANITIZE_STRING))
        {
            throw new Exception("Tentative d'injection de code via prenom (attaque sécurité)");
        }

        if(!isset($nom))
        {
            throw new Exception("Champ nom manquant");
        }
        if($nom != filter_var($nom, FILTER_SANITIZE_STRING))
        {
            throw new Exception("Tentative d'injection de code via nom (attaque sécurité)");
        }

        Validation::val_email($email);

        if(!isset($mdp))
        {
            throw new Exception("Champ mot de passe manquant");
        }
        if($mdp != filter_var($mdp, FILTER_SANITIZE_STRING))
        {
            throw new Exception("Tentative d'injection de code via mot de passe (attaque sécurité)");
        }

        $bio = filter_var($bio, FILTER_SANITIZE_STRING);
    }
    
    static function val_connexion($email, $mdp)
    {
        if(!isset($email)||$email="")
            throw new Exception("Erreur : Champ email manquant");
        if(filter_var($email, FILTER_VALIDATE_EMAIL))
            throw new Exception("Erreur : Tentative d'injection de code via l'email (attaque sécurité)");

        if(!isset($mdp)||$mdp="")
            throw new Exception("Erreur : Champ mot de passe manquant");
        if($mdp != filter_var($mdp, FILTER_SANITIZE_STRING))
            throw new Exception("Erreur : Tentative d'injection de code via le mot de passe (attaque sécurité)");
    }

    ///////////////////10 CHAMPS DANS USER//////////////////////

    //1
    static function val_idUser($idUser)
    {
        if(!isset($idUser))
            throw new Exception("Erreur : ID User manquant");
        if(!filter_var($idUser, FILTER_VALIDATE_INT))
            throw new Exception("Erreur : Tentative d'injection de code via l'ID User (attaque sécurité)");
    }

    //2
    static function val_typeUser($typeUser)
    {
        if(!isset($typeUser))
            throw new Exception("Erreur : TypeUser manquant");
        if(!filter_var($typeUser, FILTER_VALIDATE_INT))
            throw new Exception("Erreur : Tentative d'injection de code via typeUser (attaque sécurité)");
        if($typeUser < 1 || $typeUser > 5)
            throw new Exception("Erreur : Format typeUser invalide");
    }

    //3
    static function val_mdpUser($mdpUser)
    {
        if(!isset($mdpUser))
            throw new Exception("Erreur : TypeUser manquant");
        if($mdpUser != filter_var($mdpUser, FILTER_SANITIZE_STRING))
            throw new Exception("Erreur : Tentative d'injection de code via mdpUser (attaque sécurité)");
    }

    //4
    static function val_email($email)
    {
        if(!isset($email))
            throw new Exception("Erreur : Champ email manquant");
        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            throw new Exception("Erreur : Tentative d'injection de code via email (attaque sécurité)");
        if(!preg_match(" /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@(etu.uca.fr|uca.fr|ext.uca.fr)$/",$email))
            throw new Exception("Erreur : Format d'email invalide (etu.uca.fr/uca.fr/ext.uca.fr)");
    }

    //5
    static function val_nom($nom)
    {
        if(!isset($nom))
            throw new Exception("Erreur : Nom manquant");
        if($nom != filter_var($nom, FILTER_SANITIZE_STRING))
            throw new Exception("Erreur : Tentative d'injection de code via nom (attaque sécurité)");
    }

    //6
    static function val_prenom($prenom)
    {
        if(!isset($prenom))
            throw new Exception("Erreur : Prenom manquant");
        if($prenom != filter_var($prenom, FILTER_SANITIZE_STRING))
            throw new Exception("Erreur : Tentative d'injection de code via prenom (attaque sécurité)");
    }

    //7
    static function val_description(&$description)
    {
        if(!isset($description))
            throw new Exception("Erreur : Description manquante");
        $description = filter_var($description, FILTER_SANITIZE_STRING);
    }

    //8
    static function val_nbMessage(&$nbMessage)
    {
        if(!isset($nbMessage))
            throw new Exception("Erreur : NbMessage manquant");
        $nbMessage = filter_var($nbMessage, FILTER_SANITIZE_NUMBER_INT);
    }

    //9
    static function val_avatar($avatar)
    {
        if($avatar != filter_var($avatar, FILTER_SANITIZE_STRING))
            throw new Exception("Erreur : Tentative d'injection de code via avatar (attaque sécurité)");
    }

    //10
    static function val_isMute(&$isMute)
    {
        if(!isset($isMute))
            throw new Exception("Erreur : IsMute manquant");
        $isMute = filter_var($isMute, FILTER_SANITIZE_NUMBER_INT);
        if($isMute != 0 && $isMute != 1)
        {
            throw new Exception("Erreur : IsMute invalide");
        }
    }

    ////////////////////////////////////////////////////////////

    static function val_user($user)
    {
        Validation::val_idUser($user["idUser"]);
        Validation::val_typeUser($user["typeUser"]);
        Validation::val_mdpUser($user["mdpUser"]);
        Validation::val_email($user["email"]);
        Validation::val_nom($user["nom"]);
        Validation::val_prenom($user["prenom"]);
        Validation::val_description($user["description"]);
        Validation::val_nbMessage($user["nbMessage"]);
        Validation::val_avatar($user["avatar"]);
        Validation::val_isMute($user["isMute"]);
    }

    static function val_users($users)
    {
        foreach($users as $user)
        {
            Validation::val_user($user);
        }
    }
}
?>