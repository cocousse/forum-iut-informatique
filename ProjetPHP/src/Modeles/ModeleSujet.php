<?php
/**
 * Created by PhpStorm.
 * User: clthinon
 * Date: 02/03/20
 * Time: 11:04
 */

class ModeleSujet
{
    private $SujetG;
    /**
     * ModeleSujet constructor.
     */


    public function __construct()
    {
        global $dsn, $login, $mdp;
        $this->SujetG = new GatewaySujet(new Connection($dsn, $login, $mdp));
    }

    public function getSujet($matiere)
    {
        return $this->SujetG->getSujet($matiere);
    }


    public function ajouterSujet($nom, $email)
    {
        $this->SujetG->addSousTopic($nom, $email);
    }

    public function suppSujet($idUser){
        $this->SujetG->delSousTopicByidUser($idUser);
    }


}