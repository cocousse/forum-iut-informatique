<?php

class ModeleUtilisateur
{
    private $UserGateway;

    public function __construct()
    {
        global $dsn, $login, $mdp;
        $this->UserGateway = new GatewayUtilisateur(new Connection($dsn, $login, $mdp));
    }

    public function Connexion($email, $mdp)
    {
        Validation::val_connexion($email, $mdp);
        if($this->UserGateway->Connexion($email, $mdp))
        {
            $_SESSION['email'] = $email;
            $_SESSION['type'] = $this->getType($email);
        }
        else
        {
            throw new Exception("Erreur : Mot de passe ou email invalide");
        }
    }

    public function Deconnexion()
    {
        session_destroy();
        unset($_SESSION);
        session_start();
        header('location: index.php');
    }

    public function isUser()
    {
        if(isset($_SESSION['email']))
        {
            $email = $_SESSION['email'];
            Validation::val_email($email);
            return $this->UserGateway->findByEmail($email);
        }
        else
            return null;
    }

    public function AjouterUtilisateur($user)
    {
        $clair = $user->getMdp();
        $crypte = password_hash($clair, PASSWORD_DEFAULT);
        $user->setMdp($crypte);
        $this->UserGateway->insererUser($user);
    }

    public function findByEmail($email)
    {
        $vals = $this->UserGateway->findByEmail($email);
        foreach($vals as $val)
        {
            $Utilisateur = new Utilisateur();
            $Utilisateur->setType($val["typeUser"]);
            $Utilisateur->setMdp($val["mdpUser"]);
            $Utilisateur->setEmail($val["email"]);
            $Utilisateur->setNom($val["nom"]);
            $Utilisateur->setPrenom($val["prenom"]);
            $Utilisateur->setDescription($val["description"]);
            $Utilisateur->setNbMessage($val["nbMessage"]);
        }
        return $Utilisateur;
    }

    public function changerTypeUser($idUser,$type)
    {
        $this->UserGateway->changerTypeUser($idUser,$type);
    }

    public function getEmail()
    {
        if(isset($_SESSION['email']))
        {
            return $_SESSION['email'];
        }
    }

    public function getType($email)
    {
        $type = $this->UserGateway->getType($email);
        $type = $type[0]['typeUser'];
        Validation::val_typeUser($type);
        return $type;
    }

    public function getUsers()
    {
        $users = $this->UserGateway->findUsers();
        Validation::val_users($users);
        return $users;
    }

    public function getnamebyID($idUser)
    {
        Validation::val_idUser($idUser);
        return $this->UserGateway->getnamebyID($idUser);
    }

    public function banUser($idUser)
    {
        Validation::val_idUser($idUser);
        $this->UserGateway->deleteUser($idUser);
    }
    public function getIsMute($idUser)
    {
        Validation::val_idUser($idUser);
        return $this->UserGateway->getIsMute($idUser);
    }

    public function muteUser($idUser)
    {
        Validation::val_idUser($idUser);
        $this->UserGateway->muteUser($idUser);
    }

}