<?php
/**
 * Created by PhpStorm.
 * User: sigrellier
 * Date: 25/11/19
 * Time: 11:45
 */

class Utilisateur
{
    private $type;
    private $mdp;
    private $email;
    private $nom;
    private $prenom;
    private $description;
    private $nbMessage;
    
    //Constructeur d'utilisateur (type, mdp, email obligatoires)
    //A FAIRE : LES CONSTRUCTEURS DIFFERENTS
    /*public function __construct($type, $mdp, $email, $nom, $prenom, $description, $nbMessage)
    {
        $this->type=$type;
        $this->mdp=$mdp;
        $this->email=$email;
        $this->nom=$nom;
        $this->prenom=$prenom;
        $this->description=$description;
        $this->nbMessage=$nbMessage;
        
        insererUser($this);
    }*/

    function getType() {
        return $this->type;
    }

    function getMdp() {
        return $this->mdp;
    }

    function getEmail() {
        return $this->email;
    }

    function getNom() {
        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    function getDescription() {
        return $this->description;
    }

    function getNbMessage() {
        return $this->nbMessage;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setMdp($mdp) {
        $this->mdp = $mdp;
    }

    function setEmail($email) {

        $this->email = $email;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setNbMessage($nbMessage) {
        $this->nbMessage = $nbMessage;
    }
     
    public function check($type):bool {
        if ($type==$this->type){
            return true;
        }
        else return false;
    }
}

?>
