<?php
/**
 * Created by PhpStorm.
 * User: clthinon
 * Date: 17/02/20
 * Time: 10:11
 */

class ModeleCommentaire
{
    private $CommentaireG;

    /**
     * ModeleCommentaire constructor.
     * @param $CommentaireG
     */
    public function __construct()
    {
        global $dsn, $login, $mdp;
        $this->CommentaireG = new GatewayCommentaire(new Connection($dsn, $login, $mdp));
    }


    public function ecrireCPb($message,$nom,$prenom, $id)
    {
        $this->CommentaireG->ecrireCpb($message,$nom,$prenom, $id);
    }

    public function listerCommentaire($idSousSujet)
    {
        return $this->CommentaireG->listerCpb($idSousSujet);
    }

    public function suppCommentaires($idUser){
        $this->CommentaireG->deleteCommentaires($idUser);
    }

}