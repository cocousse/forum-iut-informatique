<?php

class ModeleTopic
{
    private $TopicsG;

    public function __construct()
    {
        global $dsn, $login, $mdp;
        $this->TopicsG = new GatewayTopic(new Connection($dsn, $login, $mdp));
    }

    public function getTopics()
    {
        return $this->TopicsG->getTopics();
    }

    public function ajouterTopic($nom, $email){
        $this->TopicsG->addTopic($nom, $email);
    }

    public function getIdByName($matiere)
    {
        $this->TopicsG->getIdByName($matiere);
    }
}