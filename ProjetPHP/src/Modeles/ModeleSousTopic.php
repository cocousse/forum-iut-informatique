<?php

class ModeleSousTopic
{
    private $SousTopicsG;

    public function __construct()
    {
        global $dsn, $login, $mdp;
        $this->SousTopicsG = new GatewaySousTopic(new Connection($dsn, $login, $mdp));
    }

    public function getSousTopics()
    {
        return $this->SousTopicsG->getSousTopics();
    }

    public function ajouterSousTopic($nom, $email)
    {
        $this->SousTopicsG->addSousTopic($nom, $email);
    }
}