<?php
/**
 * Created by PhpStorm.
 * User: alfarjas
 * Date: 03/02/20
 * Time: 11:34
 */

class ModeleAnnonce
{
    private $AnonnceG;
    private $modeleUtilisateur;

    public function __construct()
    {
        global $dsn, $login, $mdp;
        $this->AnonnceG = new GatewayAnnonce(new Connection($dsn, $login, $mdp));
        $this->modeleUtilisateur=new ModeleUtilisateur();
    }

    public function getAnnonce()
    {
        $tab=$this->AnonnceG->getAnnonce();
        return$tab;
    }

    public function getLast3Annonces(){
        $tab3Annonces=$this->AnonnceG->getLast3AnnoncesByDate();


        foreach ($tab3Annonces as $key=>$values){
            $id=$values[3];
            $tabNames=$this->modeleUtilisateur->getnamebyID($id);
            foreach ($tabNames as $keyb=>$valuesb){
                array_push($tab3Annonces,$valuesb[0],$valuesb[1]);
            }

        }
        //var_dump($tab3Annonces);
        return $tab3Annonces;
    }


}