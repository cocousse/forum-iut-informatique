
<?php
/**
 * Created by PhpStorm.
 * User: clthinon
 * Date: 25/11/19
 * Time: 11:39
 */

class Annonce
{
    private $titre;
    private $texte;

    /**
     * Annonce constructor.
     * @param $titre
     * @param $texte
     */
    public function __construct($titre, $texte)
    {
        $this->titre = $titre;
        $this->texte = $texte;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre): void
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * @param mixed $texte
     */
    public function setTexte($texte): void
    {
        $this->texte = $texte;
    }

}