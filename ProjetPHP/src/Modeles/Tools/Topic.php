
<?php
/**
 * Created by PhpStorm.
 * User: clthinon
 * Date: 25/11/19
 * Time: 11:39
 */

class Topic
{
    private $idTopic;
    private $nom;
    private $idUser;

    public function __construct($idTopic, $nom, $idUser)
    {
        $this->idTopic = $idTopic;
        $this->nom = $nom;
        $this->idUser = $idUser;
    }


    public function getIdTopic()
    {
        return $this->idTopic;
    }

    public function setIdTopic($idTopic)
    {
        $this->idTopic = $idTopic;
    }


    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function getIdUser()
    {
        return $this->idUser;
    }

    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }
}