
<?php
/**
 * Created by PhpStorm.
 * User: clthinon
 * Date: 25/11/19
 * Time: 11:37
 */

class Sujet
{
    private $idSujet;
    private $titre;
    private $idUser;
    private $isPrive;
    private $idTopic;
    private $datePub;

    public function __construct()
    {

    }

    public function getIdSujet()
    {
        return $this->idSujet;
    }

    public function setIdSujet($idSujet)
    {
        $this->idSujet = $idSujet;
    }

    public function getTitre()
    {
        return $this->titre;
    }

    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    public function getIdUser()
    {
        return $this->idUser;
    }

    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    public function getisPrive()
    {
        return $this->isPrive;
    }

    public function setIsPrive($isPrive)
    {
        $this->isPrive = $isPrive;
    }

    public function getIdTopic()
    {
        return $this->idTopic;
    }

    public function setIdTopic($idTopic)
    {
        $this->idTopic = $idTopic;
    }

    public function getDatePub()
    {
        return $this->datePub;
    }

    public function setDatePub($datePub)
    {
        $this->datePub = $datePub;
    }
}