<?php


namespace modeles;


class Message
{
    private $contenu;
    private $idUserDestination;

    /**
     * Message constructor.
     * @param $contenu
     * @param $idUserDestination
     */
    public function __construct($contenu, $idUserDestination)
    {
        $this->contenu = $contenu;
        $this->idUserDestination = $idUserDestination;
    }

    /**
     * @return mixed
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param mixed $contenu
     */
    public function setContenu($contenu): void
    {
        $this->contenu = $contenu;
    }

    /**
     * @return mixed
     */
    public function getIdUserDestination()
    {
        return $this->idUserDestination;
    }

    /**
     * @param mixed $idUserDestination
     */
    public function setIdUserDestination($idUserDestination): void
    {
        $this->idUserDestination = $idUserDestination;
    }

}