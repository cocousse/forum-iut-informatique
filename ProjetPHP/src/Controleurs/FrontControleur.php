<?php

class FrontControleur {

    private $mdlUser;
    private $ctrl;
    private $listeActions_Connected;
    public function __construct()
    {
        global $vues;
        $action='';
        $vueErreur=array();

        $this->mdlUser=new ModeleUtilisateur();

        $this->listeActions_Connected = array('1'=>array('supprimerMessage','ajouterMessage','ajouterSousSujet','gestionMessagePrive','||||','conditions','profil','deconnexion','topic','ajouterTopic','ajouterTopic2','ajouterSousMenu','ajouterSousMenu2','seConnecter','vueMPb','ecrireCPb','vueSujet'), //Eleve
                                   '2'=>array('ajouterAnnonce','supprimerAnnonce'), //BDE
                                   '3'=>array('ajouterSujetPrive','supprimerSujetPrive'), //Délégué
                                   '4'=>array('ajouterSujet','ajouterSousSujet','supprimerSousSujet','mute'), //Modérateur
                                   '5'=>array('supprimerSujet','listerUtilisateurs','bannir','changerRole')); //Administrateur

        $listeActions_Unconnected = array('inscription','Connexion');

        try
        {
            //Demande si user connecté
            $user = $this->mdlUser->isUser();

            /*if(isset($_REQUEST['action']))
            {
                $action = $_REQUEST['action'];
            }*/

            if($user == null)
            {
                $this->ctrl = new ControleurUnconnected();
            }
            else
            {
                $this->ctrl = new ControleurConnected();
            }


            /*
            if($this->actionValide($action) || $action=="")
            {
                if($user == NULL)
                {
                    require $vues['Connexion'];
                }
                else
                {
                    $this->ctrl=new ControleurConnected();
                }
            }
            else
            {
                $this->ctrl=new ControleurUnconnected();
            }*/
        }
        catch (Exception $e)
        {
            $vueErreur[]=$e->getMessage();
        }
        foreach($vueErreur as $err)
        {
            if(isset($err))
            {
                require $vues['Erreur'];
                break;
            }
        }

        exit(0);
    }

    public function actionValide($action)
    {
        if($action == null)
            return TRUE;
        if(in_array($action, $this->listeActions_Connected['1']))
            return TRUE;
        if(in_array($action, $this->listeActions_Connected['2']))
            return TRUE;
        if(in_array($action, $this->listeActions_Connected['3']))
            return TRUE;
        if(in_array($action, $this->listeActions_Connected['4']))
            return TRUE;
        if(in_array($action, $this->listeActions_Connected['5']))
            return TRUE;

        $changeType = explode("_",$action);
        if($changeType[0] == "changer")

            return TRUE;
        return FALSE;
    }
}
