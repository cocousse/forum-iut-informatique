<?php

class ControleurConnected
{
    private $modeleUser;
    private $modeleTopic;
    private $modeleCommentaire;
    private $modeleSujet;
    private $modeleAnnonce;
    private $prenom;
    private $rang;
    private $type;

    public function __construct()
    {
        global $vues;
        try
        {
            $action = null;
            $this->modeleTopic = new ModeleTopic();
            $this->modeleUser = new ModeleUtilisateur();
            $this->modeleCommentaire=new ModeleCommentaire();
            $this->modeleSujet=new ModeleSujet();
            $this->modeleAnnonce = new ModeleAnnonce();
            $this->setupUser();
            if(isset($_REQUEST['action']))
            {
                $action = $_REQUEST['action'];
            }

            preg_match('/(changer)|(bannir)|(mute)/', $action, $result);
            if($result != null) {
                if ($result[0] == "changer") {
                    $action = "changerRole";
                }
                if ($result[0] == "bannir") {
                    $action = "bannir";
                }
                if ($result[0] == "mute") {
                    $action = "mute";
                }
            }


            switch($action)
            {
                case NULL :
                    $this->afficherAccueil();
                    break;
                case 'profil' :
                    $this->afficherProfils();
                    break;
                case 'conditions' :
                    require $vues['Conditions'];
                    break;
                case 'deconnexion' :
                    $this->Deconnexion();
                    break;
                case 'topic' :
                    $this->afficherTopics();
                    break;
                case 'ajouterTopic' :
                    require $vues['AjouterTopic'];
                    break;
                case 'ajouterTopic2' :
                    $this->ajouterTopic();
                    break;
                case 'ajouterSousMenu' :
                    require $vues['AjouterSousMenu'];
                    break;
                case 'ajouterSousMenu2' :
                    $this->ajouterSousTopic();
                    break;
                case 'seConnecter' :
                    $this->Connexion();
                    $this->setupUser();
                    $this->afficherAccueil();
                    break;
                case 'listerUtilisateurs' :
                    $this->listerUtilisateurs();
                    break;
                case 'vueSujet':
                    $this->listerSujet();
                    break;
                case 'vueCPb':
                    $this->listerCommentairePB();
                    break;
                case 'ecrireCPb':
                    $this->entrerCommentairePB();
                    $this->listerCommentairePB();
                    break;
                case 'bannir' :
                    $this->bannir();
                    break;
                case 'mute' :
                    $this->mute();
                    break;
                case 'changerRole' :
                    $this->changerTypeUser();
                    break;



            }
        }
        catch (Exception $e)
        {
            //Si erreur de redirection
            $vueErreur[] = $e->getMessage();
        }
        if (isset($vueErreur[0]))
        {
            require $vues['Erreur'];
        }
        exit(0);
    }

    private function droitModo(){
        $user = $this->modeleUser->isUser();
        if(current($user)[1] != "4" && current($user)[1] != "5")
            throw new Exception("Erreur 403 : Vous n'avez pas accès à cette fonctionnalité.");
    }

    private function afficherAccueil()
    {
        global $vues;
        $type = $this->type;
        $rang = $this->rang;
        $prenom = $this->prenom;
        $tabAnnonces=$this->modeleAnnonce->getLast3Annonces();
        require $vues['Accueil'];
    }

    private function afficherProfils()
    {
        global $vues;
        $rang = $this->rang;
        $prenom = $this->prenom;
        $user = $this->modeleUser->isUser();
        foreach($user as $key=>$values)
        {
            $email = $values[3];
            $nom = $values[4];
            $prenom = $values[5];
            $bio = $values[6];
            $nbMessages = $values[7];
        }
        require $vues['Profil'];
    }

    public function afficherTopics()
    {
        global $vues;
        $rang = $this->rang;
        $prenom = $this->prenom;
        $type = $this->type;
        $Topic=$this->modeleTopic->getTopics();
        $categ=[];
        foreach ($Topic as $value){
            array_push($categ,$value[3]);
            if (isset($categ[$value[3]])){
                array_push($categ[$value[3]],$value[1]);
            }else{
                $categ[$value[3]]=[$value[1]];
            }

        }
        require $vues['Topic'];
    }

    public function ajouterTopic()
    {
        global $vues;
        $rang = $this->rang;
        $prenom = $this->prenom;
        if(isset($_REQUEST['nomTopic']))
        {
            $nom = $_REQUEST['nomTopic'];
        }
        $email = $this->modeleUser->getEmail();

        $this->modeleTopic->ajouterTopic($nom, $email);
        require $vues['Accueil'];
    }

    public function ajouterSousTopic()
    {
        global $vues;
        $rang = $this->rang;
        $prenom = $this->prenom;
        if(isset($_REQUEST['nomSousMenu']))
        {
            $nom = $_REQUEST['nomSousMenu'];
        }
        $email = $this->modeleUser->getEmail();
        $this->modeleSousTopic->ajouterSousTopic($nom, $email);
        require $vues['Accueil'];
    }

    public function setupUser()
    {
        $user = $this->modeleUser->isUser();
        if(isset($user))
        {
            $type = current($user)[1];
            $prenom = current($user)[5];
            $this->prenom = $prenom;

            $this->type = $type;
            switch ($type)
            {
                case 1 :
                    $this->rang = "Eleve";
                    break;
                case 2 :
                    $this->rang = "BDE";
                    break;
                case 3 :
                    $this->rang = "Délégué";
                    break;
                case 4 :
                    $this->rang = "Modérateur";
                    break;
                case 5 :
                    $this->rang = "Administrateur";
                    break;
            }
        }
    }

    function Connexion()
    {
        if(isset($_REQUEST['email']) && isset($_REQUEST['motdepasse']))
        {
            $email = $_REQUEST['email'];
            $mdp = $_REQUEST['motdepasse'];
        }
        else
            throw new Exception("Erreur : Champ(s) manquant(s)");
        $this->modeleUser->Connexion($email, $mdp);
    }

    function Deconnexion()
    {
        $this->modeleUser->Deconnexion();
    }

    private function listerUtilisateurs()
    {
        global $vues;
        $tabUsers = $this->modeleUser->getUsers();
        $user = $this->modeleUser->isUser();
        require $vues['ListeUtilisateurs'];
    }

    private function entrerCommentairePB()
    {
        if(isset($_REQUEST['Message'])){
            $message=$_REQUEST['Message'];
        }
        else
            throw new Exception("Entrez un message");
        $user = $this->modeleUser->isUser();
        $id=current($user)[0];
        $prenom=$this->prenom;
        $nom=current($user)[4];
        $this->modeleCommentaire->ecrireCPb($message,$nom,$prenom,$id);
    }

    private function listerCommentairePB()
    {
        global $vues;
        $Commentaire = $this->modeleCommentaire->listerCommentaire(1);
        $count = count($Commentaire);
        require $vues['MessagePublic'];
    }


    private function listerSujet()
    {
        global $vues;
        $matiere=$_REQUEST["matiere"];
        $idTopic=$this->modeleTopic->getIdByName($matiere);
        $sujet=$this->modeleSujet->getSujet($idTopic);
        require $vues['vueSujet'];
    }

    private function bannir(){
        if(isset($_REQUEST['action']))
            $banRequest = $_REQUEST['action'];
        else
            $banRequest = null;
        $banRequest = explode("_", $banRequest);
        $this->modeleUser->banUser($banRequest[1]);
        //$this->modeleSousTopic->suppSousTopics($banRequest[1]);
        $this->modeleCommentaire->suppCommentaires($banRequest[1]);
        header("Refresh:0; url=index.php?action=listerUtilisateurs");
    }


    private function mute()
    {
        if(isset($_REQUEST['action']))
            $muteRequest = $_REQUEST['action'];
        else
            $muteRequest = null;
        $muteRequest = explode("_", $muteRequest);
        $muteRequest = $muteRequest[1];
        $this->modeleUser->muteUser($muteRequest);
        header("Refresh:0; url=index.php?action=listerUtilisateurs");

    }

    private function changerTypeUser(){
        if(isset($_REQUEST['action']))
            $changeType = $_REQUEST['action'];
        else
            $changeType = null;
        $changeType = explode("_", $changeType);
        switch($changeType[1])
        {
            case NULL :
                break;
            case 'Admin' :
                $this->modeleUser->changerTypeUser($changeType[2],5);
                break;
            case 'Modo' :
                $this->modeleUser->changerTypeUser($changeType[2],4);
                break;
            case 'BDE' :
                $this->modeleUser->changerTypeUser($changeType[2],3);
                break;
            case 'Delegue' :
                $this->modeleUser->changerTypeUser($changeType[2],2);
                break;
            case 'Eleve' :
                $this->modeleUser->changerTypeUser($changeType[2],1);
                break;
        }
        header("Refresh:0; url=index.php?action=listerUtilisateurs");
    }

}