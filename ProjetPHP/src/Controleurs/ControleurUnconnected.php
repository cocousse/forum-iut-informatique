<?php

class ControleurUnconnected
{
    private $modele;

    function __construct()
    {
        //RECUPERATION DES VUES GLOBALES
        global $vues;

        //MODEL UTILISATEUR
        $this->modele = new ModeleUtilisateur();

        //INITIALISATION DU TABLEAU VUE ERREURS
        $vueErreur = array();

        $action = null;

        try
        {
            if(isset($_REQUEST['action']))
            {
                $action = $_REQUEST['action'];
            }
            switch ($action)
            {
                case 'inscription' :
                    require $vues['Inscription'];
                    break;
                case 'inscription2' :
                    $this->Inscription();
                    require $vues['Connexion'];
                    break;
                case 'seConnecter' :
                    $this->Connexion();
                    header("Refresh:0; url=index.php");
                    break;
                default :
                    require $vues['Connexion'];
                    break;
            }
        }
        catch (Exception $e)
        {
            //Si erreur de redirection
            $vueErreur[] = $e->getMessage();
        }

        if(isset($vueErreur[0])){
            require $vues['Erreur'];
        }

        exit(0);
    }

    function Connexion()
    {
        if(isset($_REQUEST['email']) && isset($_REQUEST['motdepasse']))
        {
            $email = $_REQUEST['email'];
            $mdp = $_REQUEST['motdepasse'];
        }
        else
            throw new Exception("Champs manquants");
        $this->modele->Connexion($email, $mdp);
    }

    function Inscription()
    {
        $user = new Utilisateur();
        //Récupération des variables pour ajouter, et validation
        $prenom = $_POST['prenom'];
        $nom = $_POST['nom'];
        $email = $_POST['email'];
        $mdp = $_POST['mdp'];
        $mdp2 = $_POST['mdp2'];
        $bio = $_POST['bio'];
        if (isset($_POST['image']))
        {
            $image = $_POST['image'];
            echo $image;
        }
        if(isset($_FILES))
            echo var_dump($_FILES);
        if($mdp != $mdp2)
        {
            throw new Exception("Erreur : Mots de passe différents");
        }

        //Seule endroit où on valide dans le contrôleur (avant d'insérer dans $user)
        Validation::val_inscription($prenom, $nom, $email, $mdp, $bio);
        $user->setPrenom($prenom);
        $user->setNom($nom);
        $user->setEmail($email);
        $user->setMdp($mdp);
        $user->setDescription($bio);

        $user->setType(1);
        $user->setNbMessage(0);

        //Donne l'instruction d'ajouter au modèle puis à la gateway
        $this->modele->AjouterUtilisateur($user);
    }
}
