<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <!--<link rel="shortcut icon" href="assets/images/favicon/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="assets/images/favicon/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicon/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-touch-icon-180x180.png" /> -->

    <title>Forum IUT : Post</title>
    <link href="Vues/css/cssMessage/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <[endif]-->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900|Dancing+Script:400,700|Raleway:400,100,300,700,900|Reenie+Beanie&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <link href="Vues/css/cssMessage/font-awesome.min.css" rel="stylesheet">
    <link href="Vues/css/cssMessage/pe-icon-7-stroke.css" rel="stylesheet">

    <link href="Vues/css/cssMessage/animate.css" rel="stylesheet">
    <link href="Vues/css/cssMessage/plugins.css" rel="stylesheet">
    <link href="Vues/css/cssMessage/style.css" rel="stylesheet">

</head>

<body class="top-navigation pushy-right-side">
    

    <!-- Site Overlay -->
    <div class="site-overlay"></div>

    <div id="master-wrapper">
        <div class="navbar nav-wrapper smoothie">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center mb30">
                        <a class="logo" href="?action=topic"><img alt="" class="logo img-responsive" src="Vues/images/logo-light.png"></a>
                    </div>
                    <div class="col-sm-12 text-center">
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-center">
                                <li class="dropdown">
                                    <a href="?action=topic" role="button" aria-haspopup="true" aria-expanded="false">Home</a></li>
                                <li class="dropdown">
                                    <a href="?action=profil" role="button" aria-haspopup="true" aria-expanded="false">Profil</a></li>
                                <li class="dropdown">
                                    <a href="#" role="button" aria-haspopup="true" aria-expanded="false">Message Privé</a></li>
                                <li class="dropdown">
                                    <a href="?action=deconnexion" role="button" aria-haspopup="true" aria-expanded="false">Deconnexion</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="white-wrapper">
            <div class="section-inner">
                <div class="containeroui">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="section-title mb20">Need Help !! J'arrive pas en php</h1>
                            <div class="item-metas text-muted mb30 white">
                                <span class="meta-item"><i class="pe-icon pe-7s-folder"></i> POSTE DANS <span class="secondary-font">Web</span></span>
                                <span class="meta-item"><i class="pe-icon pe-7s-ticket"></i> CATEGORIE <span class="secondary-font">PHP</span></span>
                                <span class="meta-item"><i class="pe-icon pe-7s-user"></i> PAR <span class="secondary-font">Antoine</span></span>
                                <span class="meta-item"><i class="pe-icon pe-7s-comment"></i> COMMENTS <span class="secondary-font">2</span></span>
                            </div>
                   			<p class="lead">Probleme en PHP pour le projet 1 : ToDoList</p>
                            <div class="oui">         
                            <p>Bonjour, j'ai un problème sur le projet php todolist, je n'arrive pas a faire la connexion.
                                <br>Please help</p>
                            </div>
                            <div class="mb90 clearfix"></div>

                            <div id="comments-list" class="wow fadeIn" data-wow-delay="0.2s">

                                <?php
                                if(isset($Commentaire )&& isset($count)){
                                    echo "<div class=\"mt60 mb50 single-section-title\">
                                    <h3>$count Commentaire(s)</h3>
                                    </div>";
                                    foreach ($Commentaire as $value){
                                        echo "<div class=\"media\">
                                    <div class=\"pull-left\">
                                        <!--<img class=\"avatar comment-avatar\" src=\"http://lorempixel.com/g/80/80/people/9\" alt=\"\">-->
                                    </div>
                                    <div class=\"media-body\">
                                        <div class=\"well\">
                                            <div class=\"media-heading\">
                                                <span class=\"heading-font\">$value[5] $value[6]</span> <small>$value[4]</small>
                                            </div>
                                            <p>$value[1]</p>
                                        </div>
                                    </div>
                                </div>";
                                    }
                                    }else{
                                        echo "<div class=\"mt60 mb50 single-section-title\">
                                    <h3>0 Comments</h3>
                                </div>";
                                }?>

                                <div id="comments-form" class="row wow fadeIn" data-wow-delay="0.2s">
                                    <div class="col-md-12">
                                        <div class="mt60 mb50 single-section-title">
                                            <h3>Repondre :</h3>
                                        </div>
                                        <div id="message"></div>
                                        <form method="post" action="?action=ecrireCPb" id="commentform" class="comment-form">
                                            <textarea name="Message" class="form-control mb40" id="comments" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                                            <input class="btn btn-primary pull-right mt30" type="submit" placeholder="Repondre">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer-wrapper" class="dark-wrapper">
            <div class="container">
                <div class="row mb90">
                    <div class="col-md-3 col-xs-6">
                        <div class="text-widget widget">
                            <div class="widget-content">
                                <img alt="" class="img-responsive" src="Vues/images/logo-light.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="text-widget widget">
                            <h4 class="widget-title mb40">Location</h4>
                            <div class="widget-content">
                                <p>Campus des Cézeaux</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="useful-link-widget widget">
                            <h4 class="widget-title mb40">Membres</h4>
                            <div class="widget-content">
                                <p>Romain Lagorsse</p>
                                <p>Franck Graffi</p>
                                <p>Corentin Cousse</p>
                                <p>Alexandre Farjas</p>
                                <p>Clément Thinon</p>
                                <p>Siméon Gréllier</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="mailing-widget widget">
                            <h4 class="widget-title mb40">Mailing List</h4>
                            <div class="content-wiget">
                                <p>Romain.LAGORSSE@etu.uca.fr</p>
                                <p>Franck.GRAFFI@etu.uca.fr</p>
                                <p>Corentin.COUSSE@etu.uca.fr</p>
                                <p>Alexandre.FARJAS@etu.uca.fr</p>
                                <p>Clément.THINON@etu.uca.fr</p>
                                <p>Siméon.GRéLLIER@etu.uca.fr</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <a href="#" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    </div>

    <script src="Vues/js/jsMessage/jquery.min.js"></script>
    <script src="Vues/js/jsMessage/bootstrap.min.js"></script>
    <script src="Vues/js/jsMessage/owl-carousel.js"></script>
    <script src="Vues/js/jsMessage/plugins.js"></script>
    <script src="Vues/js/jsMessage/init.js"></script>

</body>
</html>