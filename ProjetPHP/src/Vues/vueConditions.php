<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="Vues/css/cssConditions/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
    <link rel="stylesheet" media="all" href="Vues/css/cssConditions/design.css"/>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
    <link rel="icon" type="image/png" href="Vues/images/icons/favicon.ico"/>
    <title>Forum IUT - Conditions</title>
    <meta name="viewport" content="width=device-width"/>
</head>
<style type="text/css">
    body {
        background-image: url("Vues/images/header.jpg");
        background-size: cover;
        background-attachment: fixed;
    }
</style>
<body>
<div class="wrapper2">
    <input class="o" type="checkbox" id="test1">
    <div class="test2">
        <a href="index.php" class="btnAccueil" style="background-image: url('Vues/images/home.ico')"></a>
        <p class="side">Accueil</p>
        <a href="?action=profil" class="btnAccueil" style="background-image: url('Vues/images/oui.png')"></a>
        <p class="side">Profil</p>
        <a href="?action=topic" class="btnAccueil" style="background-image: url('Vues/images/forum.png')"></a>
        <p class="side">Forum</p>
    </div>
    <label class="test3" for="test1" style="background-image: url('Vues/images/burger.png')"></label>
    <div class="d1">
        <p class="p1">Conditions d'utilisations :</p>
        <article>
            <h2>I - LE FORUM</h2>
            <h3>1 – FONCTIONNEMENT DU FORUM</h3>
            Le forum de l'IUT Informatique de Clermont Ferrand est un espace de discussion dans lequel sont répertoriés des sujets constitués de différents messages ajoutés par les contributeurs. L’identification de l’auteur d’un sujet ou d’un message est impérative.
            out sujet ou message publié dans le forum devra impérativement être signé par votre nom, prénom (lequel ne doit être ni injurieux, ni grossier, ni ne porter atteinte à l’image de l'IUT).<br><br>
            <h4>• Mise en ligne des messages</h4>
            Par le biais de cette charte, vous acceptez la mise en ligne gratuite de vos sujets ou messages. Tout message créé sera automatiquement publié sur le forum mais s’il contrevient à la présente charte il sera supprimé.
            <br><br>
            <h4>• Suppression d’un sujet ou message</h4>
            En tant que contributeur vous avez également la possibilité de supprimer vos sujets ou messages.<br><br>
            <h4>• Les messages privés</h4>
            Les messages privés sont des messages envoyés entre contributeurs, ils ne sont visibles que par l’émetteur et le destinataire. Ces messages utilisent une messagerie du forum, vos adresses courriels ne sont jamais divulguées par l'IUT.<br><br>


            <h3>2 - PRINCIPES RÉDACTIONNELS DU FORUM</h3>
            L’espace contributif du forum est destiné à échanger au sujet de la langue française et notamment à propos d’usage des mots, d’étymologie ou de difficultés linguistiques. Ces discussions doivent être conformes à la qualité des articles du forum et pour ce faire il convient de respecter les principes de base ci-dessous :<br><br>
            <h4>• Principe 1. Avoir le souci d’ouvrir l’esprit du lecteur et ne pas l’emprisonner dans une vision restrictive et, notamment, partisane de celui-ci (pas de publicité ou de promotions etc.).</h4>
            Cet objectif écarte d'emblée :</br>
            • les textes militants ;</br>
            • les textes faisant du prosélytisme ;</br>
            • les textes publicitaires ;</br>
            • les textes promotionnels ;</br>
            • les textes et contributions personnels, qui ne viendraient pas éclairer un propos, et dont la finalité serait purement « auto-promotionnelle ».</br>  <br>
            <h4>• Principe 2. Vérifier que votre question n’est pas déjà présente dans le forum ou que la réponse n’est pas déjà notée sur le forum. </h4>
            Ce principe a pour objectif de limiter le nombre de sujets en doublons, de d’éviter de perdre l’internaute entre plusieurs fils de discussion qui traiteraient du même sujet. De tels sujets et messages pourraient être supprimés ou déplacés par la modération dans le but de simplifier la lecture et compréhension du forum. </br></br>
            <h4>• Principe 3. Un seul sujet par discussion </h4>
            Ne formulez qu'une seule question par fil de discussion. Si vous avez plus d'une question, ouvrez un fil pour chacune d'elles. Si vous désirez discuter d'un sujet connexe mais différent de la question posée dans le premier message du fil, ouvrez également un nouveau sujet.
            Le cross posting, technique consistant à poster un même sujet sur plusieurs forums, est prohibé.</br></br>
            <h4>• Principe 4. Pas de hors sujet, ni de digressions</h4>
            Si vous désirez discuter d'un autre sujet ou envoyer à un membre un commentaire à propos d'un autre sujet, veuillez utiliser les messages privés. De même, ne dupliquez pas les fils et n'ouvrez qu'un seul fil pour une même question.</br>
            <br>
            <h4>Principe 5. Veiller à rendre le texte de votre message accessible au plus grand nombre et viser une qualité de l'expression qui permette une lecture aisée de votre message.</h4>
            Le texte doit être écrit en français/anglais. Le vocabulaire utilisé dans les textes doit être adapté au sujet et au public. Néanmoins, il faut essayer d'utiliser à la fois le langage le plus juste, mais aussi le plus partagé (celui qui est compris par le plus grand nombre).<br>
            Soyez descriptif mais succinct dans vos messages. Fournissez des phrases complètes et des informations sur le contexte dans chacune de vos questions. Des éclaircissements sont fortement recommandés dans le cas de vocabulaire technique. Tout lien externe doit être accompagné d'un commentaire sur le contenu de la page en question. Ne postez pas de liens sans aucune explication.
            Les titres des sujets ne doivent inclure que le mot ou la partie de la phrase qui pose problème.<br><br>
            <h4>Principe 6. Evitez les textes trop longs.</h4>
            Vous devez poser clairement votre question, et proposer votre propre tentative de réponse. S'agissant ici de textes à vocation didactique et pédagogique, on cherchera à privilégier la simplicité qui va souvent de pair avec la clarté. On s'efforcera donc : <br>
            • d'écrire en phrases sobres et bien construites ;<br>
            • de respecter la grammaire et l’orthographe (accords, concordance des temps notamment) ;<br>
            • de soigner la ponctuation ;<br>
            • d'utiliser les majuscules à bon escient ;<br>
            • d’éviter les messages trop longs, la taille de chaque message est limitée à 1 500 caractères.<br><br>
            <h4>Principe 7. Respecter les obligations légales en matière de propriété intellectuelle.</h4>
            <h4>Principe 8. Ne pas porter atteinte à la dignité humaine, ne pas développer des propos ou des discours illicites, respecter la vie privée des personnes et leur droit à l’image.</h4>
        </article>
        <article>
            <h2>3 - PROPRIÉTÉ INTELLECTUELLE</h2>
            La publication de votre contribution doit être faite dans le respect des dispositions légales et plus particulièrement dans le respect du droit d’auteur.
            </br></br>
            Vous devez respecter le droit d’auteur, ainsi vous ne devez pas :</br>
            - contrevenir à des clauses qui lient un auteur à un éditeur ou un média, quel qu’il soit ;</br>
            - utiliser des documents enfreignant le droit d’auteur d’un tiers.</br>
            Il vous appartient de prouver, le cas échéant, que vous  avez l’autorisation d’utiliser un document protégé par le droit d’auteur. <br>
            Lorsque le document appartient au domaine public, n’oubliez pas de citer l’auteur et la source.<br><br>
            <h3>Règles d’utilisation des images</h3>
            • Si vous êtes l’auteur de cette image : elle vous appartient, vous autorisez seulement la mise en ligne gratuite de votre image. Toute utilisation devra faire l’objet d’une demande préalable. <br>
            • Si vous n’êtes pas l’auteur de cette image :<br>
            - soit l’image appartient à un tiers, dans ce cas vous ne pouvez pas l’utiliser sans avoir demandé  l’autorisation préalable à son auteur ;<br>
            - soit l’image appartient au domaine public : vous pouvez utiliser librement cette image sous réserve de citer le nom de l’auteur et sa source ;<br>
            - soit l’image est sous licence libre (creative commons, licence GNU, etc) : vous devez respecter les conditions de cette licence.<br>
            <br>
            Par ailleurs, vous devez respecter le droit à l’image des personnes.
            <br>
            Toute image représentant une marque ou un produit commercial reconnaissable sera écarté par la modération de l'IUT<br>
            En tant qu’hébergeur, l'IUT a pour obligation de retirer tout contenu à caractère contrefaisant dès qu’il en aura connaissance.<br><br>
            <h3>Règle d’utilisation des citations</h3>
            La citation ne peut être admise que si :</br>
            - elle indique clairement le nom de l’auteur et la source ;<br>
            - elle poursuit des fins didactiques. Ce qui veut dire qu’elle doit être incorporée à d’autres développements qui ont un but pédagogique, critique, scientifique ou d’information ;<br>
            - elle est courte. Cette brièveté est appréciée de façon absolue mais aussi par rapport à l’œuvre citée et par rapport à l’œuvre citante. Ainsi, la reproduction d’une œuvre en format réduit ou pendant une courte durée ne peut être considérée comme une citation. (Art. L.112-5 du code de la propriété littéraire et artistique).<br><br>
            <h3>Responsabilité</h3>
            Chaque auteur, en tant qu’éditeur de contenu, prend l’entière responsabilité des contenus qu’il crée dans l’espace contributif.
            Ainsi, vous êtes légalement responsable du contenu que vous ajoutez.</br>
            L'IUT en tant qu’hébergeur ne peut voir sa responsabilité engagée du fait des informations stockées, si elle n’avait pas effectivement connaissance de leur caractère illicite ou si dès le moment où elle en a eu cette connaissance, elle a agi promptement pour retirer ces données ou en rendre l’accès impossible.
            </p>
        </article>
        <article>
            <h2>4 – LA MODERATION</h2>
            <p ID="moderation" Name="moderation">
                Le rôle de la modération est de veiller au bon fonctionnement du forum et plus particulièrement au respect de la Charte qui constitue la règle commune aux internautes contributeurs.</br>
                Cette mission est confiée à un collectif de modérateurs. Il a pour objectif d’assurer que tout contenu déposé en contribution est conforme aux principes rédactionnels et iconographiques définis par la Charte.</br>
                <br>
                En conséquence, le collectif de modérateurs pourra :</br>
                - supprimer tout contenu hors sujet ou non conforme à la Charte ou hors sujet, notamment: articles auto-promotionnels ;
            </p>
        </article>

        <article>
            <h2>5 – RESPONSABILITÉ DE L’HÉBERGEUR</h2>
            <p ID="hebergeur" Name="hebergeur">
                En tant qu’hébergeur, l'IUT se réserve la faculté de supprimer immédiatement et sans mise en demeure préalable tout contenu qui serait contraire à la loi française, porterait atteinte à l'ordre public français, ou aux droits d'un tiers. En particulier, et sans que cette liste soit limitative, tout contenu qui pourrait :</br>
                - porter atteinte à l'image de l'IUT par l'intermédiaire de messages, textes ou images provocants (contenu pornographique, raciste, négationniste, illicite, en propageant des rumeurs, des informations fausses, en faisant du prosélytisme, en manquant de neutralité…) ;</br>
                - porter atteinte au respect de la vie privée des personnes, internautes connectés sur le forum de l'IUT. A ce titre vous vous engagez à vous abstenir de diffuser au sein des services et espaces contributifs qui vous sont proposés des messages à caractère injurieux, insultant, diffamant, dénigrant, dégradant ou calomnieux ;</br>
                - porter atteinte d'une quelconque manière aux utilisateurs mineurs du forum, de les inciter à se mettre en danger d'une quelconque manière ;</br>
                - constituer une violation des droits de propriété intellectuelle (notamment en matière de photographie et textes) ou tout autre droit de propriété appartenant à autrui.<br><br>
            </p>
            <h3>Radiation d’un contributeur</h3>
            L'IUT se réserve également le droit, sans mise en demeure préalable et sans compensation, d’interdire temporairement ou définitivement la possibilité à un contributeur de participer à l'ajout de commentaires.</br>
        </article>
        <article>
            <h2>6 - LA GESTION DES MOTS DE PASSE</h2>
            Chaque contributeur est responsable de la garde du mot de passe qui lui sera attribué pour l'accès du forum <br>
            Vous devrez prendre toutes les mesures de précaution et de sécurité nécessaires à la protection de votre mot de passe et votre nom d’utilisateur. <br>
            En cas de perte ou de vol de votre mot de passe et/ou de votre nom d’utilisateur, ou si vous prenez connaissance de l'utilisation de votre mot de passe et de votre nom d’utilisateur par un tiers non autorisé, vous devez immédiatement nous contacter à l’adresse suivante : contact@forum.uca.fr
            </br></br>
            Dans ce cadre, vous pourrez :
            - soit solliciter le remplacement de vos mots de passe et login s'il existe un risque que ceux-ci soient utilisés par un tiers ;
            - soit solliciter la restitution sécurisée de vos mots de passe et nom d’utilisateur.
            </br></br>
            L'IUT ne pourra être tenu responsable de l'utilisation frauduleuse de votre mot de passe et de votre nom d’utilisateur par un tiers qui serait parvenu à vous les subtiliser. <br>
            De plus, l'IUT se réserve le droit de vous demander de modifier votre login ou mot de passe dans le cas où l'IUT se rendrait compte d’une quelconque utilisation frauduleuse de ceux-ci.
            </p>
        </article>
    </div>



    <script src="Vues/js/jsConditions/jquery-3.3.1.slim.min.js"></script>
    <script src="Vues/js/jsConditions/popper.min.js" ></script>
    <script src="Vues/js/jsConditions/bootstrap.min.js"></script>
    <script src="Vues/js/jsConditions/scroll-animate.js"></script>
</body>
</html>
