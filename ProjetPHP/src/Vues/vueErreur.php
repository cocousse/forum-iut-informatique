<html>
<head>
    <!-- Informations sur la page -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="Vues/images/icons/favicon.ico"/>

    <!-- Le Titre -->
    <title>Forum IUT - Erreur</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- Autres -->
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/starter-template/">


</head>
<style type="text/css">
    body {
        background-image: url("Vues/images/header.jpg");
        background-size: cover;
        background-attachment: fixed;
    }
</style>
<body>
<main role="main" class="container">
    </br>
    <?php $cpt=1; foreach($vueErreur as $msg){?>
    <div class="starter-template">
        <p class="lead">
        <div class="alert alert-warning" role="alert">
            <?php echo $msg;?>
        </div>
        <?php } ?>
        </p>
    </div>
</main>
</body>
</html>