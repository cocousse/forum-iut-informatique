<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
    <!--<link rel="stylesheet" media="screen and (max-width: 800px)" href="smallscreen.css" type="text/css">-->
    <title>Forum IUT - Connexion</title>


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="Vues/images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Vues/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Vues/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Vues/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Vues/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Vues/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Vues/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Vues/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Vues/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Vues/css/cssConnexion/util.css">
    <link rel="stylesheet" type="text/css" href="Vues/css/cssConnexion/main.css">


</head>

<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST" action="?action=seConnecter">
					<span class="login100-form-title p-b-34">
						Connexion
					</span>

                <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
                    <input required id="first-name" class="input100" type="text" placeholder="E-mail" name="email">
                    <span class="focus-input100"></span>
                </div>
                <div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
                    <input required class="input100" type="password"  placeholder="Mot de passe" name="motdepasse">
                    <span class="focus-input100"></span>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Connexion
                    </button>
                </div>

                <div class="w-full text-center p-t-27 p-b-239">

                </div>

                <div class="w-full text-center">
                    <a href="?action=inscription" class="txt3">
                        Inscription
                    </a>
                </div>
            </form>

            <div class="login100-more" style="background-image: url('Vues/images/iutco2.png');"></div>
        </div>
    </div>
</div>



<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="Vues/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="Vues/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="Vues/vendor/bootstrap/js/popper.js"></script>
<script src="Vues/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="Vues/vendor/select2/select2.min.js"></script>
<script>
    $(".selection-2").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });
</script>
<!--===============================================================================================-->
<script src="Vues/vendor/daterangepicker/moment.min.js"></script>
<script src="Vues/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="Vues/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="Vues/js/jsConnexion/main.js"></script>

</body>
</html>
