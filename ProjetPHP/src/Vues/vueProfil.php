<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Civic - CV Resume">
    <meta name="keywords" content="resume, civic, onepage, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forum IUT - Profil</title>
    <link rel="icon" type="image/png" href="Vues/images/icons/favicon.ico"/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="Vues/css/cssProfil/bootstrap.css"/>
    <link rel="stylesheet" href="Vues/css/cssProfil/profil.css"/>
</head>
<body>
<section class="hero-section spad">
    <input class="o" type="checkbox" id="test1">
    <div class="test2">
        <a href="index.php" class="btnAccueil" style="background-image: url('Vues/images/home.ico')"></a>
        <p class="side">Accueil</p>
        <a href="?action=profil" class="btnAccueil" style="background-image: url('Vues/images/oui.png')"></a>
        <p class="side">Profil</p>
        <a href="?action=topic" class="btnAccueil" style="background-image: url('Vues/images/forum.png')"></a>
        <p class="side">Forum</p>
    </div>
    <label class="test3" for="test1" style="background-image: url('Vues/images/burger.png')"></label>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-10 offset-xl-1">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero-text">
                            <h2><?php if(isset($nom) && isset($prenom)) echo $nom," ",$prenom;?></h2>
                            <p><?php if(empty($bio) || !isset($bio)) echo "Biographique d'utilisateur manquante"; else echo $bio;?></p>
                        </div>
                        <div class="hero-info">
                            <h2>Informations</h2>
                            <ul>
                                <li><span>E-mail</span><?php if(isset($email)) echo $email;?></li>
                                <li><span>Nombre de messages </span><?php if(isset($nbMessages)) echo " ".$nbMessages;?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <figure class="hero-image">
                            <img src="Vues/images/profilDefaut.jpg" alt="5">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>