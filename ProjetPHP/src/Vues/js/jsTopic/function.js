function afficheMenu(obj){

    var idMenu     = obj.id;
    var idSousMenu = 'sous' + idMenu;
    var sousMenu   = document.getElementById(idSousMenu);

    for(var i = 1; i <= 14; i++){
        if(document.getElementById('sousmenu' + i)!= sousMenu){
            document.getElementById('sousmenu' + i).style.display = "none";
            document.getElementById(idMenu).style.color ="green";
        }
    }

    if(sousMenu){
        if(sousMenu.style.display == "block"){
            sousMenu.style.display = "none";
        }
        else{
            sousMenu.style.display = "block";
        }
    }
}