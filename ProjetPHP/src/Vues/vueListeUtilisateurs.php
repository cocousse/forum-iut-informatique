<!DOCTYPE html>
<html lang="en">

<style type="text/css">
    body {
        background-image: url("Vues/images/header.jpg");
        background-size: cover;
        background-attachment: fixed;
    }
    td {
        color: whitesmoke;
    }
</style>

<body>
<head>
    <meta charset="UTF-8">
    <title>Forum IUT - Utilisateurs</title>
    <link rel="icon" type="image/png" href="Vues/images/icons/favicon.ico"/>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="Vues/css/cssListeUsers/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="Vues/css/cssListeUsers/listeusers.css">
</head>
<input class="o" type="checkbox" id="test1">
<div class="test2">
    <a href="index.php" class="btnAccueil" style="background-image: url('Vues/images/home.ico')"></a>
    <p class="side">Accueil</p>
    <a href="?action=profil" class="btnAccueil" style="background-image: url('Vues/images/oui.png')"></a>
    <p class="side">Profil</p>
    <a href="?action=topic" class="btnAccueil" style="background-image: url('Vues/images/forum.png')"></a>
    <p class="side">Forum</p>
</div>
<label class="test3" for="test1" style="background-image: url('Vues/images/burger.png')"></label>
</br></br></br></br></br>
<div class="container">
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Identifiant</th>
                <th scope="col">Type</th>
                <th scope="col">Email</th>
                <th scope="col">Prénom</th>
                <th scope="col">Nom</th>
                <th scope="col">Messages</th>
                <?php
                    if(current($user)[1] == 5)
                        echo '<th scope="col">Changer Rôle</th>'; ?>
                <th scope="col">Mute</th>
                <?php
                    if(current($user)[1] == 5)
                        echo '<th scope="col">Bannir</th>'?>
            </tr>
        </thead>
        <tbody>
        <?php
        $i=1;
        foreach($tabUsers as $key=>$values){
            $idUser = $values[0];
            $typeU = $values[1];
            $email = $values[3];
            $nom = $values[4];
            $prenom = $values[5];
            $nbMessages = $values[7];
            $isMute = $values[9];
            if($isMute)
                $muteImage = 'Vues/images/muteon.png';
            else
                $muteImage = 'Vues/images/muteoff.png';

            echo '<tr>';
            echo '<th scope="row">'.$i.'</th>';
            echo '<td>'.$idUser.'</td>';
            switch($typeU){
                case 1:
                    echo '<td>Eleve</td>';
                    break;
                case 2:
                    echo '<td>BDE</td>';
                    break;
                case 3:
                    echo '<td>Délégué</td>';
                    break;
                case 4:
                    echo '<td>Modérateur</td>';
                    break;
                case 5:
                    echo '<td>Administrateur</td>';
                    break;
            }
            echo '<td>'.$email.'</td>';
            echo '<td>'.$prenom.'</td>';
            echo '<td>'.$nom.'</td>';
            echo '<td>'.$nbMessages.'</td>';
            if(current($user)[1] == 5){
                if($typeU != 5) { ?>
                    <td>
                        <div class="dropdown show">
                            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Liste des rôles</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <?php if ($typeU != 4) echo '<a class="dropdown-item" href="?action=changer_Modo_', $idUser, '">Modo</a>'; ?>
                                <?php if ($typeU != 3) echo '<a class="dropdown-item" href="?action=changer_BDE_', $idUser, '">BDE</a>'; ?>
                                <?php if ($typeU != 2) echo '<a class="dropdown-item" href="?action=changer_Delegue_', $idUser, '">Délégué</a>'; ?>
                                <?php if ($typeU != 1) echo '<a class="dropdown-item" href="?action=changer_Eleve_', $idUser, '">Elève</a>'; ?>
                            </div>
                        </div>
                    </td>
                    <?php
                    switch($isMute)
                    {
                        case 0:
                            echo '<td><a href="?action=mute_', $idUser ,'" class="btn" onclick="return confirm(\'Êtes-vous sûr de vouloir réduire au silence cet utilisateur ?\');"><img src="',$muteImage,'" style="height: 4vh; width: 4vh;"/></a></td>';
                            break;
                        case 1:
                            echo '<td><a href="?action=mute_', $idUser ,'" class="btn" onclick="return confirm(\'Êtes-vous sûr de vouloir unmute cet utilisateur ?\');"><img src="',$muteImage,'" style="height: 4vh; width: 4vh;"/></a></td>';
                            break;
                    }

                    echo '<td><a href="?action=bannir_', $idUser ,'" class="btn" onclick="return confirm(\'Êtes-vous sûr de vouloir bannir cet utilisateur ?\');"><img src="Vues/images/ban.png" style="height: 4vh; width: 4vh;"/></a></td>';
                }
                else
                { ?>
                    <td></td>
                    <td></td>
                    <td></td>
                <?php }
                echo '</tr>';
                echo '</tr>';
            }
            else {
                switch($isMute)
                {
                    case 0:
                        echo '<td><a href="?action=mute_', $idUser ,'" class="btn" onclick="return confirm(\'Êtes-vous sûr de vouloir réduire au silence cet utilisateur ?\');"><img src="',$muteImage,'" style="height: 4vh; width: 4vh;"/></a></td>';
                        break;
                    case 1:
                        echo '<td><a href="?action=mute_', $idUser ,'" class="btn" onclick="return confirm(\'Êtes-vous sûr de vouloir unmute cet utilisateur ?\');"><img src="',$muteImage,'" style="height: 4vh; width: 4vh;"/></a></td>';
                        break;
                }
            }
            $i++;
        }
        ?>
        </tbody>

    </table>
</div>
    <script src="Vues/js/jsListeUsers/jquery-3.3.1.slim.min.js"></script>
    <script src="Vues/js/jsListeUsers/popper.min.js" ></script>
    <script src="Vues/js/jsListeUsers/bootstrap.min.js"></script>
</body>
</html>