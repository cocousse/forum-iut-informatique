<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Forum IUT - Accueil</title>

    <link rel="icon" type="image/png" href="Vues/images/icons/favicon.ico"/>
    <!-- Normalize -->
    <link rel="stylesheet" type="text/css" href="Vues/css/cssAccueil/normalize.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="Vues/css/cssAccueil/bootstrap.css">
    <!-- Owl -->
    <link rel="stylesheet" type="text/css" href="Vues/css/cssAccueil/owl.css">
    <!-- Animate.css -->
    <link rel="stylesheet" type="text/css" href="Vues/css/cssAccueil/animate.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="Vues/fonts/font-awesome-4.1.0/css/font-awesome.min.css">
    <!-- Elegant Icons -->
    <link rel="stylesheet" type="text/css" href="Vues/fonts/eleganticons/et-icons.css">
    <!-- Main style -->
    <link rel="stylesheet" type="text/css" href="Vues/css/cssAccueil/cardio.css">

    <!-- Favicons (created with http://realfavicongenerator.net/)
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicons/apple-touch-icon-60x60.png">
    <link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="img/favicons/manifest.json">
    <link rel="shortcut icon" href="img/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#00a8ff">
    <meta name="msapplication-config" content="img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">-->

</head>

<body>
<div class="preloader">
    <img src="Vues/images/loader.gif" alt="Preloader image">
</div>

<header id="intro">
    <div class="container">
        <div class="table">
            <div class="header-text">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="light white">Page d'accueil</h3>
                        <h1 class="white typed">Bienvenue <?php if(isset($prenom) && isset($rang)) echo $prenom," (",$rang,") "; ?> sur le forum de l'IUT Informatique de Clermont</h1>
                        <span class="typed-cursor">|</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<section>
    <div class="cut cut-top"></div>
    <div class="container">
        <div class="row intro-tables">
            <div class="col-md-4">
                <div class="intro-table intro-table-hover1">
                    <h5 class="white heading hide-hover">Votre Profil</h5>
                    <div class="bottom">
                        <h4 class="white heading small-heading no-margin regular">Profil</h4>
                        <a href="?action=profil" class="btn btn-white-fill expand">Vers profil</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="intro-table intro-table-hover">
                    <h5 class="white heading hide-hover">Forum IUT</h5>
                    <div class="bottom">
                        <h4 class="white heading small-heading no-margin regular">Forum</h4>
                        <a href="?action=topic" class="btn btn-white-fill expand">Vers forum</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="intro-table intro-table-third">
                    <h5 class="white heading">Informations du jour</h5>
                    <div class="owl-testimonials bottom">
                        <?php
                        foreach ($tabAnnonces as $key=>$values){
                            $nom=$values[1];
                            $content=$values[2];
                            $auteur=$values[3];
                            echo '<div class="item">';
                            echo '<h4 style="margin-bottom: -6vh" class="white heading">'.$nom.'</h4>';
                            echo '<h5 style="margin-bottom: -5vh" class="white heading light">'.$content.'</h5>';
                            echo '<h5 class="white heading light">'.$auteur.'</h5>';
                            echo '</div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
            if(isset($type) && $type>=4)
                echo '<a href="?action=listerUtilisateurs" class="btn btn-blue ripple trial-button">Liste des utilisateurs</a>';
            ?>

        </div>
    </div>
</section>

</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 text-center-mobile">
                <h3 class="white">A propos :</h3>
                <h5 class="light regular light-white">Voici l'accueil du Forum de l'IUT Informatique de Clermont. Ici, les dernières annonces sont affichées. <br>Site réalisé par Franck Graffi, Romain Lagorsse, Corentin Cousse, Siméon Grellier, Alexandre Farjas, Clément Tinon.</h5>
                <a href="?action=conditions" class="btn btn-blue ripple trial-button">Conditions d'utilisations</a>
                <a href="?action=deconnexion" class="btn btn-blue ripple trial-button">Se Deconnecter</a>
            </div>
        </div>
        <div class="row bottom-footer text-center-mobile">
            <div class="col-sm-8">
                <p>© 2020 All Rights Reserved. Powered by the IUT exclusively for IUT members</p>
            </div>
            <!--	<div class="col-sm-4 text-right text-center-mobile">
                    <ul class="social-footer">
                        <li><a href="http://www.facebook.com/pages/Codrops/159107397912"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="http://www.twitter.com/codrops"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://plus.google.com/101095823814290637419"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>-->
        </div>
    </div>
</footer>
<!-- Holder for mobile navigation -->
<div class="mobile-nav">
    <ul>
    </ul>
    <a href="#" class="close-link"><i class="arrow_up"></i></a>
</div>

<!-- Scripts -->
<script src="Vues/js/jsAccueil/jquery-1.11.1.min.js"></script>
<script src="Vues/js/jsAccueil/owl.carousel.min.js"></script>
<script src="Vues/js/jsAccueil/bootstrap.min.js"></script>
<script src="Vues/js/jsAccueil/wow.min.js"></script>
<script src="Vues/js/jsAccueil/typewriter.js"></script>
<script src="Vues/js/jsAccueil/jquery.onepagenav.js"></script>
<script src="Vues/js/jsAccueil/main.js"></script>
</body>

</html>
