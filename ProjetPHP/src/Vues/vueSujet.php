<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link rel="icon" type="image/png" href="Vues/images/icons/favicon.ico"/>
    <!--<link rel="shortcut icon" href="assets/images/favicon/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="assets/images/favicon/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicon/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-touch-icon-180x180.png" /> -->

    <title>Forum IUT : Sujet</title>
    <link href="Vues/css/cssSujet/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <[endif]-->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900|Dancing+Script:400,700|Raleway:400,100,300,700,900|Reenie+Beanie&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <link href="Vues/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="Vues/fonts/pe-icons/css/pe-icon-7-stroke.css" rel="stylesheet">

    <link href="Vues/css/cssSujet/animate.css" rel="stylesheet">
    <link href="Vues/css/cssSujet/plugins.css" rel="stylesheet">
    <link href="Vues/css/cssSujet/style.css" rel="stylesheet">

</head>
<body class="top-navigation pushy-right-side">
    

    <!-- Site Overlay -->
    <div class="site-overlay"></div>

    <div id="master-wrapper">
        <div class="navbar nav-wrapper smoothie">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center mb30">
                        <a class="logo" href="?action=topic"><img alt="" class="logo img-responsive" src="Vues/images/logo-light.png"></a>
                    </div>
                    <div class="col-sm-12 text-center">
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-center">
                                <li class="dropdown">
                                    <a href="?action=topic" role="button" aria-haspopup="true" aria-expanded="false">Home</a></li>
                                <li class="dropdown">
                                    <a href="?action=profil" role="button" aria-haspopup="true" aria-expanded="false">Profil</a></li>
                                <li class="dropdown">
                                    <a href="#" role="button" aria-haspopup="true" aria-expanded="false">Message Privé</a></li>
                                <li class="dropdown">
                                    <a href="?action=deconnexion" role="button" aria-haspopup="true" aria-expanded="false">Deconnexion</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="white-wrapper">
            <div class="section-inner">
                <?php if(isset($matiere)&& isset($sujet)){
                    echo "<h3 class=\"zbub43 section-title\">Matière : $matiere</h3>";
                    foreach ($sujet as $pb){
                        echo "<div class=zebi43>
                    <a class=btn btn-primary btn-transparent active kebab href=?action=vueCPb data-filter=*>Accéder</a>
                    <a class=zboub43 href=#> Problème: ";echo $pb[1];echo " </a>
                    <p class=zebia43> Auteur du sujet:         Simescargot</p>
                    <p class=zebia43> Date du dernier message: 17/02/20 10h52 </p>
                    
                </div>";

                    }


                }?>


                <div class="zebi43">
                    <a class="btn btn-primary btn-transparent active kebab" href="#" data-filter="*">Accéder</a>
                    <a class="zboub43" href="#"> Problème: Je supporte l'Olympique Lyonnais </a>
                    <p class="zebia43"> Auteur du sujet:         Simescargot</p>
                    <p class="zebia43"> Date du dernier message: 17/02/20 10h52 </p>        
                </div>
                <div class="zebi43">
                    <a class="btn btn-primary btn-transparent active kebab" href="#" data-filter="*">Accéder</a>
                    <a class="zboub43" href="#"> Problème: Je vais courir à 2h30 du matin et j'attrape la crêve </a>
                    <p class="zebia43"> Auteur du sujet: Encore Simescargot</p>
                    <p class="zebia43"> Date du dernier message: 17/02/20 10h52 </p>
                </div>
                <div class="zebi43">
                    <a class="btn btn-primary btn-transparent active kebab" href="#" data-filter="*">Accéder</a>
                    <a class="zboub43" href="#"> Problème: J'ai un faible pour les frites sur le trottoire </a>
                    <p class="zebia43"> Auteur du sujet:         Simescargot</p>
                    <p class="zebia43"> Date du dernier message: 17/02/20 10h52 </p>
                </div>
            </div>
        </section>
</div>
          <footer id="footer-wrapper" class="dark-wrapper">
            <div class="container">
                <div class="row mb90">
                    <div class="col-md-3 col-xs-6">
                        <div class="text-widget widget">
                            <div class="widget-content">
                                <img alt="" class="img-responsive" src="Vues/images/logo-light.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="text-widget widget">
                            <h4 class="widget-title mb40">Location</h4>
                            <div class="widget-content">
                                <p>Campus des Cézeaux</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="useful-link-widget widget">
                            <h4 class="widget-title mb40">Membres</h4>
                            <div class="widget-content">
                                <p>Romain Lagorsse</p>
                                <p>Franck Graffi</p>
                                <p>Corentin Cousse</p>
                                <p>Alexandre Farjas</p>
                                <p>Clément Thinon</p>
                                <p>Siméon Gréllier</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="mailing-widget widget">
                            <h4 class="widget-title mb40">Mailing List</h4>
                            <div class="content-wiget">
                                <p>Romain.LAGORSSE@etu.uca.fr</p>
                                <p>Franck.GRAFFI@etu.uca.fr</p>
                                <p>Corentin.COUSSE@etu.uca.fr</p>
                                <p>Alexandre.FARJAS@etu.uca.fr</p>
                                <p>Clément.THINON@etu.uca.fr</p>
                                <p>Siméon.GRéLLIER@etu.uca.fr</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <a href="#" id="back-to-top"><i class="fa fa-angle-up"></i></a>
<script src="Vues/js/jsSujet/jquery.min.js"></script>
    <script src="Vues/js/jsSujet/bootstrap.min.js"></script>
    <script src="Vues/js/jsSujet/owl-carousel.js"></script>
    <script src="Vues/js/jsSujet/plugins.js"></script>
    <script src="Vues/js/jsSujet/init.js"></script>
</body>

</html>
