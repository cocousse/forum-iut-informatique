<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link rel="icon" type="image/png" href="Vues/images/icons/favicon.ico"/>
    <!--<link rel="shortcut icon" href="Vues/images/favicon/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="Vues/images/favicon/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="Vues/images/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="Vues/images/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="Vues/images/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="Vues/images/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="Vues/images/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="Vues/images/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="Vues/images/favicon/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="Vues/images/favicon/apple-touch-icon-180x180.png" /> -->

    <title>Forum IUT</title>
    <link href="Vues/css/cssTopic/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <[endif]-->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900|Dancing+Script:400,700|Raleway:400,100,300,700,900|Reenie+Beanie&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <link href="Vues/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="Vues/fonts/pe-icons/css/pe-icon-7-stroke.css" rel="stylesheet">

    <link href="Vues/css/cssTopic/animate.css" rel="stylesheet">
    <link href="Vues/css/cssTopic/plugins.css" rel="stylesheet">
    <link href="Vues/css/cssTopic/style.css" rel="stylesheet">

</head>

<body class="top-navigation pushy-right-side regular-nav">

    <!-- Site Overlay -->
    <div class="site-overlay"></div>

    <div id="master-wrapper">


        <div class="navbar nav-wrapper smoothie">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <a class="logo" href="#"><img alt="" class="logo img-responsive" src="Vues/images/logo-light.png"></a>
                    </div>
                    <div class="col-sm-8">
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-search"></i></a>
                                    <ul class="dropdown-menu">
                                        <form class="form-inline">
                                            <button type="submit" class="btn btn-default pull-right"><i class="glyphicon glyphicon-search"></i></button>
                                            <input type="text" class="form-control pull-left" placeholder="Search">
                                        </form>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="white-wrapper">
            <div class="section-inner">
                <div class="container">
                    <div class="row mb60 text-center">
                        <div class="col-sm-12">
                            <h3 class="section-title">Forum de l'IUT Informatique de Clermont-Ferrand</h3>
                            <p class="section-sub-title">Bienvenue <?php if (isset ($prenom)){
                                echo "$prenom";
                            }else
                                echo "dans le forum" ?> </p>
                            <section id="our-work" class="white-wrapper">
                            </section>

                            <?php

                            $i=1;
                            if(isset($categ)){

                                foreach ($categ as $key=>$item){
                                    if (isset($key[0])){


                                    echo "<div class=\"col-md-6 match-height mb30\">
                            <div class=\"hover-effect smoothie\">
                                <a href=\"#\" class=\"smoothie\">
                                    <img src=\"Vues/images/blog/";echo $key;echo ".jpg\" alt=\"Image\" class=\"img-responsive smoothie\"></a>
                                <div class=\"hover-overlay smoothie text-center\">
                                    <div class=\"vertical-align-bottom\">
                                        <h4>$key</h4>
                                        <span class=\"item-category-span\">Informatique</span>
                                    </div>
                                </div>
                                <div class=\"hover-caption dark-overlay smoothie text-center\">
                                    <div class=\"vertical-align-bottom\">
                                    ";
                                    $i++;
                                    foreach ($item as $value)
                                        if(isset($value)){

                                            echo " <a href=\"?action=vueSujet&matiere=$value\" class=\"b btn btn-primary pull-right mb20\">$value</a>";
                                        }


                                    echo "</div>
                                </div>
                            </div>
                        </div>";}
                                }
                            }else{
                                echo"";
                            }?>

            </div>
        </section>


        

        <footer id="footer-wrapper" class="dark-wrapper">
            <div class="container">
                <div class="row mb90">
                    <div class="col-md-3 col-xs-6">
                        <div class="text-widget widget">
                            <div class="widget-content">
                                <img alt="" class="img-responsive" src="Vues/images/logo-light.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="text-widget widget">
                            <h4 class="widget-title mb40">Location</h4>
                            <div class="widget-content">
                                <p>Campus des Cézeaux</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="useful-link-widget widget">
                            <h4 class="widget-title mb40">Membres</h4>
                            <div class="widget-content">
                                <p>Romain Lagorsse</p>
                                <p>Franck Graffi</p>
                                <p>Corentin Cousse</p>
                                <p>Alexandre Farjas</p>
                                <p>Clément Thinon</p>
                                <p>Siméon Gréllier</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="mailing-widget widget">
                            <h4 class="widget-title mb40">Mailing List</h4>
                            <div class="content-wiget">
                                <p>Romain.LAGORSSE@etu.uca.fr</p>
                                <p>Franck.GRAFFI@etu.uca.fr</p>
                                <p>Corentin.COUSSE@etu.uca.fr</p>
                                <p>Alexandre.FARJAS@etu.uca.fr</p>
                                <p>Clément.THINON@etu.uca.fr</p>
                                <p>Siméon.GRéLLIER@etu.uca.fr</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <a href="#" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    </div>

    <script src="Vues/js/jsTopic/jquery.min.js"></script>
    <script src="Vues/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="Vues/js/jsTopic/owl-carousel.js"></script>
    <script src="Vues/js/jsTopic/plugins.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="Vues/js/jsTopic/init.js"></script>

</body>

</html>

