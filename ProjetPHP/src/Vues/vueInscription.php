<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Forum IUT - Inscription</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="Vues/images/icons/favicon.ico"/>

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link rel="stylesheet" href="Vues/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">

    <!-- STYLE CSS -->
    <link rel="stylesheet" href="Vues/css/cssConnexion/style.css">
    <link rel="stylesheet" type="text/css" href="Vues/css/cssConnexion/main.css">
</head>

<body>

<div class="wrapper" style="background-image: url('Vues/images/backgReg.jpg');">
    <div class="inner">
        <div class="image-holder">
            <img src="Vues/images/iutco2.png" alt="">
        </div>
        <form method="POST" action="?action=inscription2">
            <h3>Formulaire Inscription</h3>
            <div class="form-group">
                <input required type="text" placeholder="Prénom" class="form-control" name="prenom">
                <input required type="text" placeholder="Nom" class="form-control" name="nom">
            </div>
            <div class="form-wrapper">
                <input required type="text" placeholder="Email Address" class="form-control" name="email">
                <i class="zmdi zmdi-email"></i>
            </div>

            <div class="form-wrapper">
                <input required type="password" placeholder="Mot de passe" class="form-control" name="mdp">
                <i class="zmdi zmdi-lock"></i>
            </div>
            <div class="form-wrapper">
                <input required type="password" placeholder="Confirmez mot de passe" class="form-control" name="mdp2">
                <i class="zmdi zmdi-lock"></i>
            </div>

            <div class="form-group">
                <textarea class="form-control bio" placeholder="Bio" id="exampleFormControlTextarea1" rows="5" cols="20" name="bio"></textarea>

            </div>
            <div class="form-wrapper">
                <input type="file" name="image"/>
            </div>

            <div class="container-login100-form-btn">
                <button class="login100-form-btn ">Inscription
                    <i class="zmdi zmdi-arrow-right"></i>
                </button>
            </div>

            <div class="container-login100-form-btn">
                <a href="?action=Connexion" class="login100-form-btn">Connexion
                    <i class="zmdi zmdi-arrow-right"></i>
                </a>
            </div>

        </form>

    </div>
</div>

</body>
</html>